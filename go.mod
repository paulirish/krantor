module gitlab.com/paulirish/krantor

go 1.20

require (
	github.com/fsnotify/fsnotify v1.7.0
	github.com/putdotio/go-putio v1.7.1
	golang.org/x/oauth2 v0.13.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.16.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
